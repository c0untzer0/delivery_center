# Marketplace Parser Service

## Visão geral

Dado os requisitos do sistema foi proposta a seguinte solução de arquitetura:

![Architecture](images/architecture.jpg)

Segundo a imagem a sistema o sistema será composto de cinco partes principais:

1. API de integração com aplicações externas (Marketplace, Food, Goods, e etc). Esta não será coberta integralmente por este projeto.
2. Broker de mensagens para prover o desacoplamento entre as aplicações e permirtir o uso de uma arquitetura [reativa](https://www.reactivemanifesto.org/pt-BR),
   ou seja, de uma [arquitetura Resiliente, Orientado a Mensagens, Elástica e Responsiva](https://www.lightbend.com/white-papers-and-reports/reactive-programming-versus-reactive-systems).
3. Banco de dados para armazenamento das ordens recebidas.
4. Aplicação para fazer o parser das ordens, salvar no banco e encaminhar para a API de processamento interno.
5. Aplicação de processamento das ordens. Essa aplicação já existe e se encontra na url https://delivery-center-recruitment-ap.herokuapp.com/.

Para simular um cenário um pouco mais realista as mensagens encaminhadas ao Broker serão codificadas via Protobuf e a aplicação que fará o parser dos dados decodificará esta mensagem e acessara os dados brutos em format JSON, fará o parser e enviará para a API da delivery-center.

## Principais frameworks e bibliotecas utilizados no desenvolvimento da aplicação

* Ecto
* Broadway
* Plug
* Jason
* Protobuf
* Credo

## Executando no Docker

### Pré requisitos

Linux ou Mac OS
Docker, Docker Compose, Make

### Instalação

À partir do diretório do projeto "```cd marketplace_parser_service/```" execute as seguintes ações:

1. Criar containeres:
```
make build
```

2. Executar aplicações dependentes
Em uma janela de terminal Linux ou Mac execute:
```
docker-compose -f infraestructure-compose.yaml up -d && docker-compose -f infraestructure-compose.yaml logs -f
```

3. Executar esquema e migrações de banco de dados:
```
make ecto
```

4. Criar a fila do Orders no RabbitMQ:

Após a execução com sucesso do passo anterior você deverá criar a fila no Broker de mensagens.

Acesse a url **http://localhost:15672** acesse a aba ***Queues***, no campo Name coloque o nome da fila como **orders** e clique no botão **Add queue**.

Abaixo a sequência descrita acima em telas:

![Overview](images/rabbitmq-overview.png)

![Queue](images/rabbitmq-create-queue.png)

![Created](images/rabbitmq-created-queue.png)

![Details](images/rabbitmq-queue-details.png)


4. E finalmente executar a aplicação principal:
```
make run
```

## Executando no K8s

Não faz parte desse projeto demonstrar como rodar o RabbitMQ ou o Postgress no Kubernetes. Apenas o serviço Elixir será demonstrado como executá-lo no Kubernetes.

### Pré requisitos

[Cluster Kubernetes](https://kubernetes.io/pt/)
[Kustomize](https://kustomize.io/)
[RabbitMQ](https://www.rabbitmq.com/) Deployment (ou [Operator](https://www.rabbitmq.com/kubernetes/operator/operator-overview.html)) e [Postgres](https://severalnines.com/database-blog/using-kubernetes-deploy-postgresql) Deployment ativos no cluster

### Configurando

Na pasta kubernetes do projeto você encontra o arquivo que contém as variáveis de ambiente para a configuração da aplicação.
Atualmente a aplicação será aplicada no namespace default do cluster kubernetes.

### Instalação

Dentro da pasta kubernetes execute:

```
kustomize build | kubectl apply -f -
```

## Observabilidade

A aplicação expõe métricas via Prometheus no endpoint **/metrics** e expõe um endpoint para health check em **/health**.
Por default o servidor http sobe na porta 8084 mas isso pode ser alterado via variável de ambiente **HTTP_PORT**.

## Pontos importante do Código:

* marketplace_parser_service/lib/marketplace_parser_service/application.ex
* marketplace_parser_service/lib/marketplace_parser_service/eip/data_ingestion.ex
* marketplace_parser_service/lib/marketplace_parser_service/eip/delivery_center_client.ex
* marketplace_parser_service/lib/marketplace_parser_service/eip/fake_ingestion_worker.ex
* marketplace_parser_service/priv/protos/protocol/delivery_center_protocol.proto
* marketplace_parser_service/compile-pb.sh
* marketplace_parser_service/infraestructure-compose.yaml
* marketplace_parser_service/kubernetes

## Pontos de Melhoria

* Adicionar documentação aos módulos.
* Melhorar tratamento de erros durante as requisições à API.
* Melhor uso do banco de dados.
* Refatoração geral.
* Mais testes.
* Adicionar mais TypeSpecs.