defmodule MarketplaceParserService do
  @moduledoc """
  Documentation for `MarketplaceParserService`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MarketplaceParserService.hello()
      :world

  """
  def hello do
    :world
  end
end
