defmodule MarketplaceParserService.OrderParser do
  @moduledoc false
  require Logger
  alias MarketplaceParserService.Order
  alias MarketplaceParserService.Order.{Address, Customer, Invoice, Item, Payment}

  @spec parse(type :: any()) :: MarketplaceParserService.Order
  def parse(original) do
    with {:ok, original, new} <- build_invoice(original),
         {:ok, original, new} <- build_address(original, new),
         {:ok, original, new} <- build_customer(original, new),
         {:ok, original, new} <- build_items(original, new),
         {:ok, _original, new} <- build_payments(original, new) do
      build_order(new)
    end
  end

  defp build_invoice(original),
    do:
      {:ok, original,
       Map.put(
         %{},
         "invoice",
         Invoice.new(
           original["id"],
           original["store_id"],
           original["total_amount"],
           original["total_shipping"],
           original["total_amount_with_shipping"],
           original["date_created"]
         )
       )}

  defp build_address(original, new) do
    address = original["shipping"]["receiver_address"]

    {:ok, original,
     Map.put(
       new,
       "address",
       Address.new(
         address["country"]["id"],
         address["state"]["name"],
         address["city"]["name"],
         address["neighborhood"]["name"],
         address["street_name"],
         address["comment"],
         address["latitude"],
         address["longitude"],
         address["zip_code"],
         address["street_number"]
       )
     )}
  end

  defp build_customer(original, new) do
    buyer = original["buyer"]
    id = buyer["id"]
    nickname = buyer["nickname"]
    email = buyer["email"]
    area = buyer["phone"]["area_code"]
    phone = buyer["phone"]["number"]

    {:ok, original,
     Map.put(
       new,
       "customer",
       Customer.new(
         id,
         nickname,
         email,
         "#{area}#{phone}"
       )
     )}
  end

  defp build_items(original, new),
    do:
      {:ok, original,
       Map.put(new, "items", Enum.map(original["order_items"], fn item -> build_item(item) end))}

  defp build_payments(original, new) do
    payments = original["payments"]

    {:ok, original,
     Map.put(new, "payments", Enum.map(payments, fn payment -> build_payment(payment) end))}
  end

  defp build_order(new),
    do: Order.new(new["invoice"], new["address"], new["customer"], new["items"], new["payments"])

  defp build_item(original_item),
    do:
      Item.new(
        original_item["item"]["id"],
        original_item["item"]["title"],
        original_item["unit_price"],
        original_item["quantity"],
        original_item["full_unit_price"],
        []
      )

  defp build_payment(original_payment),
    do: Payment.new(original_payment["payment_type"], original_payment["total_paid_amount"])
end
