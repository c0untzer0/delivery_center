defmodule MarketplaceParserService.Endpoint do
  @moduledoc """
  A Plug responsible for logging request info, parsing request body's as JSON,
  matching routes, and dispatching responses.
  """

  use Plug.Router

  plug(Plug.Logger)
  plug(MarketplaceParserService.MetricsExporter)

  plug(:match)

  plug(Plug.Parsers, parsers: [:json], json_decoder: {Jason, :decode!, [[keys: :atoms]]})

  plug(:dispatch)

  # A simple health to test that the server is up
  get "/health" do
    send_resp(conn, 200, Jason.encode!(%{"status" => "up"}))
  end

  match _ do
    send_resp(conn, 404, "Oops... Nothing here :'(")
  end
end
