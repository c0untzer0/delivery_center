defmodule MarketplaceParserService.Application do
  @moduledoc false

  use Application
  require Logger
  alias MarketplaceParserService.Metrics.Setup
  alias Vapor.Provider.{Dotenv, Env}

  @impl true
  def start(_type, _args) do
    config = load_system_env()

    Setup.setup()

    children = [
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: MarketplaceParserService.Endpoint,
        options: [port: config.http_port],
        transport_options: [
          num_acceptors: 10
        ]
      ),
      MarketplaceParserService.Repo,
      MarketplaceParserService.DeliveryCenterClient.child_spec(),
      MarketplaceParserService.DataIngestion
    ]

    # This is only for simulation purposes
    if config.start_simulator do
      children ++ [MarketplaceParserService.FakeIngestionWorker.child_spec()]
    end

    opts = [strategy: :one_for_one, name: MarketplaceParserService.Supervisor]
    Logger.debug("Starting Application. Exposing ports #{config.http_port}")
    Supervisor.start_link(children, opts)
  end

  defp load_system_env do
    providers = [
      %Dotenv{},
      %Env{
        bindings: [
          {:start_simulator, "START_SIMULATOR", default: true, required: false},
          {:http_port, "HTTP_PORT", default: 8084, map: &String.to_integer/1, required: false},
          {:http_num_acceptors, "HTTP_NUM_ACCEPTORS",
           default: System.schedulers_online(), required: false},
          {:host, "BROKER_HOST", default: "localhost", required: false},
          {:queue, "BROKER_ORDERS_QUEUE", default: "orders", required: false},
          {:username, "BROKER_USER", default: "rabbitmq", required: false},
          {:password, "BROKER_PASSWORD", default: "rabbitmq", required: false},
          {:concurrency, "PIPELINE_ORDERS_CONCURRENCY",
           default: System.schedulers_online(), required: false},
          {:url, "PIPELINE_DELIVERY_CENTER_URL",
           default: "https://delivery-center-recruitment-ap.herokuapp.com/", required: false}
        ]
      }
    ]

    config = Vapor.load!(providers)

    set_vars(config)
    config
  end

  defp set_vars(config) do
    Application.put_env(:marketplace_parser_service, :http_port, config.http_port)

    Application.put_env(
      :marketplace_parser_service,
      :http_num_acceptors,
      config.http_num_acceptors
    )

    Application.put_env(:marketplace_parser_service, :host, config.host)
    Application.put_env(:marketplace_parser_service, :queue, config.queue)
    Application.put_env(:marketplace_parser_service, :username, config.username)
    Application.put_env(:marketplace_parser_service, :password, config.password)
    Application.put_env(:marketplace_parser_service, :concurrency, config.concurrency)
    Application.put_env(:marketplace_parser_service, :url, config.url)
  end
end
