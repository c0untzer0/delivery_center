defmodule MarketplaceParserService.Order do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder,
           only: [
             :externalCode,
             :storeId,
             :subTotal,
             :deliveryFee,
             :total,
             :country,
             :state,
             :city,
             :district,
             :street,
             :complement,
             :latitude,
             :longitude,
             :dtOrderCreate,
             :postalCode,
             :number,
             :customer,
             :items,
             :payments
           ]}
  @enforce_keys [
    :externalCode,
    :storeId,
    :subTotal,
    :deliveryFee,
    :total,
    :country,
    :state,
    :city,
    :district,
    :street,
    :complement,
    :latitude,
    :longitude,
    :dtOrderCreate,
    :postalCode,
    :number,
    :customer,
    :items,
    :payments
  ]
  defstruct [
    :externalCode,
    :storeId,
    :subTotal,
    :deliveryFee,
    :total,
    :country,
    :state,
    :city,
    :district,
    :street,
    :complement,
    :latitude,
    :longitude,
    :dtOrderCreate,
    :postalCode,
    :number,
    :customer,
    :items,
    :payments
  ]

  def new(invoice, address, customer, items, payments) do
    %Order{
      externalCode: invoice.externalCode,
      storeId: invoice.storeId,
      subTotal: invoice.subTotal,
      deliveryFee: Float.to_string(invoice.deliveryFee),
      total: invoice.total,
      country: address.country,
      state: address.state,
      city: address.city,
      district: address.district,
      street: address.street,
      complement: address.complement,
      latitude: address.latitude,
      longitude: address.longitude,
      dtOrderCreate: invoice.dtOrderCreate,
      postalCode: address.postalCode,
      number: address.number,
      customer: customer,
      items: items,
      payments: payments
    }
  end
end

defmodule MarketplaceParserService.Order.Invoice do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder,
           only: [:externalCode, :storeId, :subTotal, :deliveryFee, :total, :dtOrderCreate]}
  @enforce_keys [:externalCode, :storeId, :subTotal, :deliveryFee, :total, :dtOrderCreate]
  defstruct [:externalCode, :storeId, :subTotal, :deliveryFee, :total, :dtOrderCreate]

  def new(externalCode, storeId, subTotal, deliveryFee, total, dtOrderCreate) do
    %Invoice{
      externalCode: "#{externalCode}",
      storeId: storeId,
      subTotal: Float.to_string(subTotal),
      deliveryFee: deliveryFee,
      total: Float.to_string(total),
      dtOrderCreate: dtOrderCreate
    }
  end
end

defmodule MarketplaceParserService.Order.Address do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder,
           only: [
             :country,
             :state,
             :city,
             :district,
             :street,
             :complement,
             :latitude,
             :longitude,
             :postalCode,
             :number
           ]}
  @enforce_keys [
    :country,
    :state,
    :city,
    :district,
    :street,
    :complement,
    :latitude,
    :longitude,
    :postalCode,
    :number
  ]
  defstruct [
    :country,
    :state,
    :city,
    :district,
    :street,
    :complement,
    :latitude,
    :longitude,
    :postalCode,
    :number
  ]

  def new(
        country,
        state,
        city,
        district,
        street,
        complement,
        latitude,
        longitude,
        postalCode,
        number
      ) do
    %Address{
      country: country,
      state: state,
      city: city,
      district: district,
      street: street,
      complement: complement,
      latitude: latitude,
      longitude: longitude,
      postalCode: postalCode,
      number: number
    }
  end
end

defmodule MarketplaceParserService.Order.Customer do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder, only: [:externalCode, :name, :email, :contact]}
  @enforce_keys [:externalCode, :name, :email, :contact]
  defstruct [:externalCode, :name, :email, :contact]

  def new(externalCode, name, email, contact) do
    %Customer{
      externalCode: "#{externalCode}",
      name: name,
      email: email,
      contact: contact
    }
  end
end

defmodule MarketplaceParserService.Order.Item do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder, only: [:externalCode, :name, :price, :quantity, :total, :subItems]}
  @enforce_keys [:externalCode, :name, :price, :quantity, :total, :subItems]
  defstruct [:externalCode, :name, :price, :quantity, :total, :subItems]

  def new(externalCode, name, price, quantity, total, subItems) do
    %Item{
      externalCode: "#{externalCode}",
      name: name,
      price: price,
      quantity: quantity,
      total: total,
      subItems: subItems
    }
  end
end

defmodule MarketplaceParserService.Order.Payment do
  @moduledoc false
  alias __MODULE__

  @derive {Jason.Encoder, only: [:type, :value]}
  @enforce_keys [:type, :value]
  defstruct [:type, :value]

  def new(type, value), do: %Payment{type: String.upcase(type), value: value}
end
