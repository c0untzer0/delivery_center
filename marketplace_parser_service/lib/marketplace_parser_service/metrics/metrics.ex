defmodule MarketplaceParserService.Metrics.Setup do
  @moduledoc false

  def setup do
    MarketplaceParserService.MetricsExporter.setup()
  end
end
