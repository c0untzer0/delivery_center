defmodule MarketplaceParserService.Orders do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  import Ecto.Changeset
  import Ecto.Query

  alias __MODULE__
  alias MarketplaceParserService.Repo

  schema "orders" do
    field(:order_id, :string)
    field(:status, :string)
    field(:data, :map)

    timestamps()
  end

  def changeset(orders, params \\ %{}) do
    all_fields = ~w(order_id status data)a

    orders
    |> cast(params, all_fields)
    |> validate_required(all_fields)
  end

  def insert_order(%{} = order) do
    all_fields = ~w(order_id status data)a

    %Orders{}
    |> cast(order, all_fields)
    |> validate_required(all_fields)
    |> Repo.insert!()
  end

  def get_orders do
    query =
      from(o in Orders,
        select: {o.order_id, o.status, o.data}
      )

    query
    |> Repo.all()
  end

  def get_order(id), do: Repo.get!(Orders, id)

  def get_by_order_id(order_id), do: Repo.get_by(Orders, order_id: order_id)

  def update_order(%{"id" => id} = order_changes) do
    status_field = ~w(status)a

    Repo.get!(Orders, id)
    |> cast(order_changes, status_field)
    |> Repo.update!()
  end

  def update_by_order_id(order_changes) do
    status_field = ~w(status)a

    Repo.get_by(Orders, order_id: order_changes["order_id"])
    |> cast(order_changes, status_field)
    |> Repo.update!()
  end

  def delete_order(id) do
    Repo.get!(Orders, id)
    |> Repo.delete!()
  end
end
