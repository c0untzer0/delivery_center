defmodule MarketplaceParserService.Repo do
  use Ecto.Repo,
    otp_app: :marketplace_parser_service,
    adapter: Ecto.Adapters.Postgres
end
