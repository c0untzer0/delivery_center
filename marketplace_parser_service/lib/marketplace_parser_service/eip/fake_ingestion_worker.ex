defmodule MarketplaceParserService.FakeIngestionWorker do
  @moduledoc false
  use GenServer
  use AMQP
  require Logger

  alias Delivery.Center.Protocol.Message

  @scheduler_worker_period 30_000

  @impl true
  def init(state) do
    host = Application.get_env(:marketplace_parser_service, :host)
    queue = Application.get_env(:marketplace_parser_service, :queue)
    username = Application.get_env(:marketplace_parser_service, :username)
    password = Application.get_env(:marketplace_parser_service, :password)

    work(%{state | host: host, username: username, password: password, queue: queue})
    schedule_work(@scheduler_worker_period)
    {:ok, state}
  end

  def child_spec do
    %{
      id: MarketplaceParserService.FakeIngestionWorker,
      start:
        {MarketplaceParserService.FakeIngestionWorker, :start_link,
         [%{host: nil, username: nil, password: nil, queue: nil}]}
    }
  end

  def start_link(state \\ []) do
    Logger.info("Starting Fake Source Application")
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def handle_info(:work, state) do
    work(state)

    # Reschedule once more
    schedule_work(@scheduler_worker_period)

    {:noreply, state}
  end

  defp schedule_work(time), do: Process.send_after(self(), :work, time)

  defp work(state) do
    Logger.info("Process request...")
    payload = get_order()

    message =
      Message.new(payload: payload)
      |> Message.encode()

    {:ok, conn} =
      Connection.open(
        "amqp://#{Map.get(state, :username)}:#{Map.get(state, :password)}@#{Map.get(state, :host)}"
      )

    {:ok, chan} = Channel.open(conn)

    case AMQP.Basic.publish(chan, "", Map.get(state, :queue), message) do
      :ok -> Logger.debug("Message sent successfully")
      _ -> Logger.warn("Failed to send the message")
    end

    Channel.close(chan)
    Connection.close(conn)

    state
  end

  defp get_order do
    {:ok, datetime} = DateTime.now("Etc/UTC")
    id = DateTime.to_unix(datetime)

    payload = %{
      id: id,
      store_id: 282,
      date_created: "2019-06-24T16:45:32.000-04:00",
      date_closed: "2019-06-24T16:45:35.000-04:00",
      last_updated: "2019-06-25T13:26:49.000-04:00",
      total_amount: 49.9,
      total_shipping: 5.14,
      total_amount_with_shipping: 55.04,
      paid_amount: 55.04,
      expiration_date: "2019-07-22T16:45:35.000-04:00",
      total_shipping: 5.14,
      order_items: [
        %{
          item: %{
            id: "IT4801901403",
            title: "Produto de Testes"
          },
          quantity: 1,
          unit_price: 49.9,
          full_unit_price: 49.9
        }
      ],
      payments: [
        %{
          id: 12_312_313,
          order_id: 9_987_071,
          payer_id: 414_138,
          installments: 1,
          payment_type: "credit_card",
          status: "paid",
          transaction_amount: 49.9,
          taxes_amount: 0,
          shipping_cost: 5.14,
          total_paid_amount: 55.04,
          installment_amount: 55.04,
          date_approved: "2019-06-24T16:45:35.000-04:00",
          date_created: "2019-06-24T16:45:33.000-04:00"
        }
      ],
      shipping: %{
        id: 43_444_211_797,
        shipment_type: "shipping",
        date_created: "2019-06-24T16:45:33.000-04:00",
        receiver_address: %{
          id: 1_051_695_306,
          address_line: "Rua Fake de Testes 3454",
          street_name: "Rua Fake de Testes",
          street_number: "3454",
          comment: "teste",
          zip_code: "85045020",
          city: %{
            name: "Cidade de Testes"
          },
          state: %{
            name: "São Paulo"
          },
          country: %{
            id: "BR",
            name: "Brasil"
          },
          neighborhood: %{
            id: nil,
            name: "Vila de Testes"
          },
          latitude: -23.629037,
          longitude: -46.712689,
          receiver_phone: "41999999999"
        }
      },
      status: "paid",
      buyer: %{
        id: 136_226_073,
        nickname: "JOHN DOE",
        email: "john@doe.com",
        phone: %{
          area_code: 41,
          number: "999999999"
        },
        first_name: "John",
        last_name: "Doe",
        billing_info: %{
          doc_type: "CPF",
          doc_number: "09487965477"
        }
      }
    }

    Jason.encode!(payload)
  end
end
