defmodule MarketplaceParserService.DeliveryCenterClient do
  @moduledoc false
  require Logger
  alias Finch.Response

  def child_spec do
    {Finch,
     name: __MODULE__,
     pools: %{
       :default => [size: System.schedulers_online()],
       Application.get_env(:marketplace_parser_service, :url) => [size: pool_size()]
     }}
  end

  def send_order(order) do
    Logger.debug("Send Order to API")

    order
    |> post
    |> handle_response
  end

  defp post(order) do
    {:ok, payload} = Jason.encode(order)
    url = Application.get_env(:marketplace_parser_service, :url)

    :post
    |> Finch.build(
      url,
      [{"X-Sent", set_date()}],
      payload
    )
    |> Finch.request(__MODULE__)
  end

  defp handle_response({:ok, %Response{body: body, status: status}}) do
    result =
      case status do
        200 -> {:ok, body}
        _ -> {:failure, body}
      end

    Logger.debug("API respone #{inspect(result)}")
    result
  end

  defp pool_size, do: 25

  defp set_date, do: DateTime.utc_now() |> Calendar.strftime("%Hh%M - %d/%m/%y")
end
