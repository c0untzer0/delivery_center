defmodule MarketplaceParserService.DataIngestion do
  @moduledoc false
  use Broadway

  require Logger

  alias Broadway.Message
  alias MarketplaceParserService.{DeliveryCenterClient, OrderParser, Orders}

  def start_link(_args) do
    host = Application.get_env(:marketplace_parser_service, :host)
    queue = Application.get_env(:marketplace_parser_service, :queue)
    username = Application.get_env(:marketplace_parser_service, :username)
    password = Application.get_env(:marketplace_parser_service, :password)
    concurrency = Application.get_env(:marketplace_parser_service, :concurrency)

    Broadway.start_link(__MODULE__,
      name: __MODULE__,
      producer: [
        module:
          {BroadwayRabbitMQ.Producer,
           queue: queue,
           connection: [
             username: username,
             password: password,
             host: host
           ],
           qos: [
             prefetch_count: 50
           ]},
        concurrency: concurrency
      ],
      processors: [
        default: [
          concurrency: concurrency
        ]
      ]
    )
  end

  def handle_message(_processor, %Message{data: data} = message, _context) do
    Logger.debug("Received a message #{inspect(message)}")

    with {:ok, payload} <- decode_proto(data),
         {:ok, order} <- insert(payload) do
      result =
        order
        |> OrderParser.parse()
        |> DeliveryCenterClient.send_order()

      case result do
        {:ok, response} -> handle_success(payload, response)
        {:failure, reason} -> handle_failure(payload, reason)
        _ -> Logger.warn("Unknown response status")
      end
    else
      _ -> Logger.warn("Failure during process")
    end

    message
  end

  defp handle_success(payload, _response),
    do: Orders.update_by_order_id(%{"order_id" => "#{payload["id"]}", "status" => "SUCCESS"})

  defp handle_failure(payload, _reason),
    do: Orders.update_by_order_id(%{"order_id" => "#{payload["id"]}", "status" => "FAILURE"})

  defp decode_proto(data) do
    decoded =
      data
      |> Delivery.Center.Protocol.Message.decode()
      |> extract_payload()
      |> Jason.decode!()

    {:ok, decoded}
  end

  defp insert(payload) do
    id = Map.get(payload, "id")

    %{
      order_id: "#{id}",
      status: "RECEIVED",
      data: payload
    }
    |> MarketplaceParserService.Orders.insert_order()

    {:ok, payload}
  end

  defp extract_payload(message), do: message.payload
end
