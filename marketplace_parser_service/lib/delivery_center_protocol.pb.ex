defmodule Delivery.Center.Protocol.Metadata.HeadersEntry do
  @moduledoc false
  use Protobuf, map: true, syntax: :proto3

  @type t :: %__MODULE__{
          key: String.t(),
          value: String.t()
        }

  defstruct [:key, :value]

  def descriptor do
    # credo:disable-for-next-line
    Elixir.Google.Protobuf.DescriptorProto.decode(
      <<10, 12, 72, 101, 97, 100, 101, 114, 115, 69, 110, 116, 114, 121, 18, 16, 10, 3, 107, 101,
        121, 24, 1, 32, 1, 40, 9, 82, 3, 107, 101, 121, 18, 20, 10, 5, 118, 97, 108, 117, 101, 24,
        2, 32, 1, 40, 9, 82, 5, 118, 97, 108, 117, 101, 58, 8, 8, 0, 16, 0, 24, 0, 56, 1>>
    )
  end

  field(:key, 1, type: :string)
  field(:value, 2, type: :string)
end

defmodule Delivery.Center.Protocol.Metadata do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          timestamp: integer,
          headers: %{String.t() => String.t()}
        }

  defstruct [:timestamp, :headers]

  def descriptor do
    # credo:disable-for-next-line
    Elixir.Google.Protobuf.DescriptorProto.decode(
      <<10, 8, 77, 101, 116, 97, 100, 97, 116, 97, 18, 28, 10, 9, 116, 105, 109, 101, 115, 116,
        97, 109, 112, 24, 1, 32, 1, 40, 3, 82, 9, 116, 105, 109, 101, 115, 116, 97, 109, 112, 18,
        73, 10, 7, 104, 101, 97, 100, 101, 114, 115, 24, 2, 32, 3, 40, 11, 50, 47, 46, 100, 101,
        108, 105, 118, 101, 114, 121, 46, 99, 101, 110, 116, 101, 114, 46, 112, 114, 111, 116,
        111, 99, 111, 108, 46, 77, 101, 116, 97, 100, 97, 116, 97, 46, 72, 101, 97, 100, 101, 114,
        115, 69, 110, 116, 114, 121, 82, 7, 104, 101, 97, 100, 101, 114, 115, 26, 64, 10, 12, 72,
        101, 97, 100, 101, 114, 115, 69, 110, 116, 114, 121, 18, 16, 10, 3, 107, 101, 121, 24, 1,
        32, 1, 40, 9, 82, 3, 107, 101, 121, 18, 20, 10, 5, 118, 97, 108, 117, 101, 24, 2, 32, 1,
        40, 9, 82, 5, 118, 97, 108, 117, 101, 58, 8, 8, 0, 16, 0, 24, 0, 56, 1>>
    )
  end

  field(:timestamp, 1, type: :int64)

  field(:headers, 2,
    repeated: true,
    type: Delivery.Center.Protocol.Metadata.HeadersEntry,
    map: true
  )
end

defmodule Delivery.Center.Protocol.Message do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t(),
          payload: binary,
          metadata: Delivery.Center.Protocol.Metadata.t() | nil
        }

  defstruct [:id, :payload, :metadata]

  def descriptor do
    # credo:disable-for-next-line
    Elixir.Google.Protobuf.DescriptorProto.decode(
      <<10, 7, 77, 101, 115, 115, 97, 103, 101, 18, 14, 10, 2, 105, 100, 24, 1, 32, 1, 40, 9, 82,
        2, 105, 100, 18, 24, 10, 7, 112, 97, 121, 108, 111, 97, 100, 24, 2, 32, 1, 40, 12, 82, 7,
        112, 97, 121, 108, 111, 97, 100, 18, 62, 10, 8, 109, 101, 116, 97, 100, 97, 116, 97, 24,
        3, 32, 1, 40, 11, 50, 34, 46, 100, 101, 108, 105, 118, 101, 114, 121, 46, 99, 101, 110,
        116, 101, 114, 46, 112, 114, 111, 116, 111, 99, 111, 108, 46, 77, 101, 116, 97, 100, 97,
        116, 97, 82, 8, 109, 101, 116, 97, 100, 97, 116, 97>>
    )
  end

  field(:id, 1, type: :string)
  field(:payload, 2, type: :bytes)
  field(:metadata, 3, type: Delivery.Center.Protocol.Metadata)
end
