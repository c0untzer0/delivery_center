defmodule MarketplaceParserService.Repo.Migrations.CreateOrdersTable do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :order_id, :string
      add :status, :string
      add :data, :map

      timestamps()
    end

    create index(:orders, [:status])
    create index(:orders, [:order_id])

  end
end
