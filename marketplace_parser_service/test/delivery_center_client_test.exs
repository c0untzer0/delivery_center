defmodule DeliveryCenterClientTest do
  use ExUnit.Case
  require Logger
  alias MarketplaceParserService.DeliveryCenterClient, as: Client

  test "call remote api" do
    order = %MarketplaceParserService.Order{
      city: "Cidade de Testes",
      complement: "teste",
      country: "BR",
      customer: %MarketplaceParserService.Order.Customer{
        contact: "41999999999",
        email: "john@doe.com",
        externalCode: "136226073",
        name: "JOHN DOE"
      },
      deliveryFee: 5.14,
      district: "Vila de Testes",
      dtOrderCreate: "2019-06-24T16:45:32.000-04:00",
      externalCode: "9987071",
      items: [
        %MarketplaceParserService.Order.Item{
          externalCode: "IT4801901403",
          name: "Produto de Testes",
          price: 49.9,
          quantity: 1,
          subItems: [],
          total: 49.9
        }
      ],
      latitude: -23.629037,
      longitude: -46.712689,
      number: "3454",
      payments: [%MarketplaceParserService.Order.Payment{type: "CREDIT_CARD", value: 55.04}],
      postalCode: "85045020",
      state: "São Paulo",
      storeId: 282,
      street: "Rua Fake de Testes",
      subTotal: 49.9,
      total: 55.04
    }

    response = Client.send_order(order)
    Logger.info("Result: #{inspect(response)}")
    assert {:failure, _} = response
  end
end
