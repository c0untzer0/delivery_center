defmodule MarketplaceParserService.RepoCase do
  @moduledoc false
  use ExUnit.CaseTemplate
  alias Ecto.Adapters.SQL.Sandbox

  using do
    quote do
      alias MarketplaceParserService.Repo

      import Ecto
      import Ecto.Query
      import MarketplaceParserService.RepoCase
    end
  end

  setup tags do
    :ok = Sandbox.checkout(MarketplaceParserService.Repo)

    unless tags[:async] do
      Sandbox.mode(MarketplaceParserService.Repo, {:shared, self()})
    end

    :ok
  end
end
