defmodule MarketplaceParserService.MixProject do
  use Mix.Project

  def project do
    [
      app: :marketplace_parser_service,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: [aliases()]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {MarketplaceParserService.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:vapor, "~> 0.10.0"},
      {:jason, "~> 1.2"},
      {:protobuf, "~> 0.7.1"},
      {:broadway_rabbitmq, "~> 0.6.5"},
      {:ecto_sql, "~> 3.5"},
      {:postgrex, "~> 0.15.8"},
      {:castore, "~> 0.1.0"},
      {:finch, "~> 0.6.3"},
      {:plug_cowboy, "~> 2.3"},
      {:prometheus, "~> 4.6"},
      {:prometheus_plugs, "~> 1.1"},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp aliases do
    [
      test: ["ecto.create", "ecto.migrate", "test"]
    ]
  end
end
