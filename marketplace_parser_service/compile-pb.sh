#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

# CloudState Protocol
protoc --elixir_out=gen_descriptors=true,plugins=grpc:./lib --proto_path=priv/protos/protocol/ priv/protos/protocol/delivery_center_protocol.proto
