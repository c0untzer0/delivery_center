use Mix.Config

config :protobuf, extensions: :enabled

# Logger general configuration
config :logger,
  backends: [:console],
  compile_time_purge_matching: [
    [level_lower_than: :debug]
  ]

# Logger Console Backend-specific configuration
config :logger, :console,
  format: "$date $time [$node]:[$metadata]:[$level]:$levelpad$message\n",
  metadata: [:pid]

# Application general configuration
config :marketplace_parser_service,
  http_port: 8084,
  http_num_acceptors: 10,
  queue: "orders",
  username: "rabbitmq",
  password: "rabbitmq",
  host: "localhost",
  marketplace_url: "https://delivery-center-recruitment-ap.herokuapp.com/"

# Application Ecto and Database driver configuration
config :marketplace_parser_service, ecto_repos: [MarketplaceParserService.Repo]

config :marketplace_parser_service, MarketplaceParserService.Repo,
  database: "marketplace_parser_service",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"
