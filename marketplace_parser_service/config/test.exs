use Mix.Config

config :marketplace_parser_service, MarketplaceParserService.Repo,
  database: "marketplace_parser_service",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
